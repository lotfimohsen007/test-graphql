
//general types
export interface IPageInfo {
    endCursor?: string;
    hasNextPage: boolean;
    startCursor?: string;
    hasPreviousPage: boolean;
}

export interface ITotalCount {
    value: number;
    isAccurate: boolean;
}

export interface IUser {
    username: string;
    ssoId: string;
    firstName: string;
    lastName: string;
    email: string;
    image: string;
}

// aggregated reaction's type

export interface IAggregatedReactionsValue {
    count: number;
    value: number;
}
export interface IAggregatedReactionsType {
    type?: string;
    values: IAggregatedReactionsValue[];
}
export interface IAggregatedReaction {
    objectId?: string;
    postId?: string;
    types: IAggregatedReactionsType[];
}

// post's type

export interface IPost {
    id: string;
    owner: IUser;
    status: 'published';
    content: string;
    type: string;
    flag: IFlag;
    tags: string[];
    objectId: string;
    objectAddressExtensions: JSON;
    order: number;
    extensions: JSON;
    createdAt: string;
    updatedAt: string;
    lastActivityAt: string;
    userAudience: IUser;
    // groupAudience: Group
    replies: IPostResult;
    replyTo: IPost;
    aggregatedReactions: IAggregatedReaction;
    currentUserReactions: IReaction[];
    // flag: Flag
}
export interface IPostResult {
    items: IPost[];
    pageInfo: IPageInfo;
    totalCount: ITotalCount;
}

// reaction's type

export interface IReaction {
    id: string;
    type?: string;
    objectId?: string;
    post: IPost;
    user?: IUser;
    value: number;
}
export interface IReactionResult {
    items: IReaction[];
    pageInfo?: IPageInfo;
    totalCount: ITotalCount;
}
export interface ISetReactionValueResult {
    createdReaction: IReaction;
    deletedReaction: IReaction;
}

//customized types

export interface IBookAggregatedReaction {
    aggregatedReactions: {
        types: IAggregatedReactionsType[];
    }[];
}
export interface IBookListAggregatedReactions {
    aggregatedReactions: {
        bookSlug?: string;
        types: IAggregatedReactionsType[];
    }[];
}

export interface IBookReviews {
    allPosts: IPostResult;
}

export interface IUserReaction {
    userPost: IPostResult;
    userRate: IReactionResult;
}

export interface IUserReview {
    posts: IPostResult;
}

export interface IRate {
    reactions: IReactionResult;
}

export interface IReviewsPageInfo {
    from: 'after' | 'before';
    hashId: string;
}

export interface ISetReactionToBookMutation {
    setRate: ISetReactionValueResult;
    setReview: IPost;
}
export interface IBookDeleteReaction {
    deleteReaction: IReaction;
    deletePost: IPost;
}

export interface IWriteReply {
    createPost: IPost;
}

export interface IDeleteReply {
    deletePost: IPost;
}

export interface IUpdateReply {
    updatePost: IPost;
}

export interface IReactionToPost {
    setReactionValue: ISetReactionValueResult;
}

export interface IDeletePostReaction {
    deleteReaction: IReaction;
}

export interface ICurrentPost {
    post: IPost;
}

export interface IWriteFlag {
    createFlag: IFlag;
}
export interface IFlag {
    user: IUser;
    post: IPost;
    type: FlagType;
    description: string;
    createdAt: string;
}

export type FlagType =
    | 'SPAM'
    | 'NUDITY'
    | 'HATE_SPEECH'
    | 'VIOLENCE'
    | 'DRUG'
    | 'HARASSMENT'
    | 'COPYRIGHT'
    | 'OTHER';
