import gql from 'graphql-tag';
export const GET_BOOK_REACTIONS = gql`
    query getBookReactions($objectId: String!) {
        aggregatedReactions(types: ["rate"], objectIds: [$objectId]) {
            types {
                values {
                    count
                    value
                }
            }
        }
    }
`;
