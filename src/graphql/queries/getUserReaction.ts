import gql from 'graphql-tag';
export const GET_USER_REACTION = gql`
    query getUserReaction($username: String, $groupId: String, $objectId: String) {
        userPost: posts(
            filter: {
                groupAudienceId: $groupId
                objectId: $objectId
                owner: $username
                replyToId: null
            }
        ) {
            items {
                id
                flag {
                    user {
                        username
                    }
                    post {
                        id
                    }
                    type
                    description
                    createdAt
                }
                owner {
                    username
                    image
                    firstName
                    lastName
                }
                content
                aggregatedReactions(types: ["like"]) {
                    types {
                        values {
                            count
                            value
                        }
                    }
                }

                replyTo {
                    id
                }
                currentUserReactions(types: ["like"]) {
                    id
                    value
                    objectId
                    post {
                        id
                    }
                    user {
                        username
                    }
                    type
                }
                createdAt
                updatedAt
                replies {
                    items {
                        id
                        flag {
                            user {
                                username
                            }
                            post {
                                id
                            }
                            type
                            description
                            createdAt
                        }
                        owner {
                            username
                            image
                            firstName
                            lastName
                        }
                        content
                        aggregatedReactions(types: ["like"]) {
                            types {
                                values {
                                    count
                                    value
                                }
                            }
                        }
                        replyTo {
                            id
                        }
                        currentUserReactions(types: ["like"]) {
                            id
                            value
                            objectId
                            post {
                                id
                            }
                            user {
                                username
                            }
                            type
                        }
                        createdAt
                        updatedAt
                    }
                    pageInfo {
                        endCursor
                        hasNextPage
                        startCursor
                        hasPreviousPage
                    }
                    totalCount {
                        value
                    }
                }
            }
            pageInfo {
                endCursor
                hasNextPage
                startCursor
                hasPreviousPage
            }
            totalCount {
                value
            }
        }
        userRate: reactions(
            filter: { type: "rate", objectId: $objectId, username: $username }
        ) {
            items {
                id
                value
                objectId
                post {
                    id
                }
                user {
                    username
                }
                type
            }
            totalCount {
                value
            }
        }
    }
`;
