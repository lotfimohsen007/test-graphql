import gql from 'graphql-tag';
export const DELETE_BOOK_REACTION = gql`
    mutation deleteBookReaction($rateId: ID!, $reviewId: ID!) {
        deletePost(id: $reviewId) {
            id
            content
            owner {
                image
                username
                firstName
                lastName
            }
            aggregatedReactions(types: ["like"]) {
                types {
                    values {
                        count
                        value
                    }
                }
            }
            currentUserReactions(types: ["like"]) {
                id
                value
                objectId
                post {
                    id
                }
                user {
                    username
                }
                type
            }
            replies {
                items {
                    id
                    owner {
                        username
                        image
                        firstName
                        lastName
                    }
                    content
                    aggregatedReactions(types: ["like"]) {
                        types {
                            values {
                                count
                                value
                            }
                        }
                    }
                    replyTo {
                        id
                    }
                    currentUserReactions(types: ["like"]) {
                        id
                        value
                        objectId
                        post {
                            id
                        }
                        user {
                            username
                        }
                        type
                    }
                    createdAt
                    updatedAt
                }
                pageInfo {
                    endCursor
                    hasNextPage
                    startCursor
                    hasPreviousPage
                }
                totalCount {
                    value
                }
            }
            createdAt
            updatedAt
            replyTo {
                id
            }
        }
        deleteReaction(id: $rateId) {
            id
            value
            objectId
            post {
                id
            }
            user {
                username
            }
            type
        }
    }
`;
