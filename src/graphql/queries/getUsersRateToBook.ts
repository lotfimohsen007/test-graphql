import gql from 'graphql-tag';
export const GET_USERS_RATE_TO_BOOK = gql`
    query($bookId: String, $usersList: [ReactionFilter!]) {
        reactions(filter: { objectId: $bookId, type: "rate", OR: $usersList }) {
            items {
                id
                value
                objectId
                post {
                    id
                }
                user {
                    username
                }
                type
            }
            totalCount {
                value
            }
        }
    }
`;
