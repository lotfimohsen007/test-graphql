import gql from "graphql-tag";

export const GET_BOOK_REVIEWS = gql`
  query getBookReviews($groupId: String, $objectId: String) {
    allPosts: posts(
      filter: {
        groupAudienceId: $groupId
        objectId: $objectId
        replyToId: null
      }
    ) {
      items {
        id
        owner {
          username
          image
          firstName
          lastName
        }
        content
        aggregatedReactions(types: ["like"]) {
          types {
            values {
              count
              value
            }
          }
        }

        replyTo {
          id
        }
        currentUserReactions(types: ["like"]) {
          id
          value
          objectId
          post {
            id
          }
          user {
            username
          }
          type
        }
        createdAt
        updatedAt
        replies(take: 10) {
          items {
            id
            owner {
              username
              image
              firstName
              lastName
            }
            content
            aggregatedReactions(types: ["like"]) {
              types {
                values {
                  count
                  value
                }
              }
            }
            replyTo {
              id
            }
            currentUserReactions(types: ["like"]) {
              id
              value
              objectId
              post {
                id
              }
              user {
                username
              }
              type
            }
            createdAt
            updatedAt
          }
          pageInfo {
            endCursor
            hasNextPage
            startCursor
            hasPreviousPage
          }
          totalCount {
            value
          }
        }
      }
      pageInfo {
        endCursor
        hasNextPage
        startCursor
        hasPreviousPage
      }
      totalCount {
        value
      }
    }
  }
`;
