import gql from 'graphql-tag';
export const SET_REACTION_TO_BOOK = gql`
    mutation reactionToBook(
        $bookId: String!
        $content: String!
        $groupId: String
        $value: Int!
    ) {
        setReview: createPost(
            content: $content
            objectId: $bookId
            groupAudienceId: $groupId
            replyToId: null
        ) {
            id
            content
            owner {
                image
                username
                firstName
                lastName
            }
            aggregatedReactions(types: ["like"]) {
                types {
                    values {
                        count
                        value
                    }
                }
            }
            currentUserReactions(types: ["like"]) {
                id
                value
                objectId
                post {
                    id
                }
                user {
                    username
                }
                type
            }
            replies {
                items {
                    id
                    owner {
                        username
                        image
                        firstName
                        lastName
                    }
                    content
                    aggregatedReactions(types: ["like"]) {
                        types {
                            values {
                                count
                                value
                            }
                        }
                    }
                    replyTo {
                        id
                    }
                    currentUserReactions(types: ["like"]) {
                        id
                        value
                        objectId
                        post {
                            id
                        }
                        user {
                            username
                        }
                        type
                    }
                    createdAt
                    updatedAt
                }
                pageInfo {
                    endCursor
                    hasNextPage
                    startCursor
                    hasPreviousPage
                }
                totalCount {
                    value
                }
            }
            createdAt
            updatedAt

            replyTo {
                id
            }
        }

        setRate: setReactionValue(type: "rate", objectId: $bookId, value: $value) {
            deletedReaction {
                id
                value
                objectId
                post {
                    id
                }
                user {
                    username
                }
                type
            }
            createdReaction {
                id
                value
                objectId
                post {
                    id
                }
                user {
                    username
                }
                type
            }
        }
    }
`;
