import { useMutation } from '@apollo/client';
import {
    IAggregatedReactionsType,
    IPost,
    IPostResult,
    IReaction,
    IReactionResult,
    ISetReactionToBookMutation,
} from '../interface';
import { SET_REACTION_TO_BOOK } from '../mutations/reactionToBook';

export const useReactionToBook = (
    completedHandle?: (data: ISetReactionToBookMutation) => void,
) => {
    const result = useMutation<ISetReactionToBookMutation>(SET_REACTION_TO_BOOK, {
        update(cache, { data: { setRate, setReview } }) {
            // update posts list
            cache.modify({
                fields: {
                    posts: (existing: IPostResult, { readField, toReference }) => {
                        const totalCount = existing.totalCount.value;
                        let modifiedItems: IPost[] = existing.items;
                        modifiedItems = [
                            toReference(cache.identify(setReview as any)) as any,
                            ...existing.items,
                        ];

                        const newObj: IPostResult = {
                            ...existing,
                            items: modifiedItems,
                            totalCount: { ...existing.totalCount, value: totalCount + 1 },
                        };

                        return newObj;
                    },
                },
            });

            //update rates list
            cache.modify({
                fields: {
                    reactions: (
                        existing: IReactionResult,
                        { readField, toReference },
                    ) => {
                        const totalCount = existing.totalCount.value;
                        let modifiedItems: IReaction[] = existing.items;
                        if (setRate.deletedReaction !== null) {
                            modifiedItems = existing.items.filter(
                                (item) => item.id !== setRate.deletedReaction.id,
                            );
                        }
                        if (setRate.createdReaction !== null) {
                            modifiedItems = [
                                toReference(
                                    cache.identify(setRate.createdReaction as any),
                                ) as any,
                                ...existing.items,
                            ];
                        }
                        const newObj: IReactionResult = {
                            ...existing,
                            items: modifiedItems,
                            totalCount: { ...existing.totalCount, value: totalCount + 1 },
                        };

                        return newObj;
                    },
                },
            });

            // update overall rates
            cache.modify({
                fields: {
                    aggregatedReactions(existing) {
                        const fakeObj = [
                            {
                                types: [
                                    {
                                        values: [
                                            {
                                                count: 0,
                                                value: 1,
                                            },
                                            {
                                                count: 0,
                                                value: 2,
                                            },
                                            {
                                                count: 0,
                                                value: 3,
                                            },
                                            {
                                                count: 0,
                                                value: 4,
                                            },
                                            {
                                                count: 0,
                                                value: 5,
                                            },
                                        ],
                                    },
                                ],
                            },
                        ];

                        const newAggReactions: {
                            types: IAggregatedReactionsType[];
                        } = JSON.parse(
                            JSON.stringify(existing?.length > 0 ? existing : fakeObj),
                        );
                        newAggReactions?.[0].types?.[0].values.forEach((item) => {
                            if (
                                setRate.deletedReaction !== null &&
                                item.value === setRate.deletedReaction.value
                            ) {
                                item.count -= 1;
                            }
                            if (
                                setRate.createdReaction !== null &&
                                item.value === setRate.createdReaction.value
                            ) {
                                item.count += 1;
                            }
                        });
                        return newAggReactions;
                    },
                },
            });
        },
        onCompleted: completedHandle,
    });

    return result;
};
