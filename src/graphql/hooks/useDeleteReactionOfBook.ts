import { useMutation } from "@apollo/client";
import {
  IAggregatedReaction,
  IBookDeleteReaction,
  IPostResult,
  IReactionResult,
} from "../interface";
import { DELETE_BOOK_REACTION } from "../queries/deleteReactionOfBook";

export const useDeleteBookReaction = (
  completedHandler?: (data: IBookDeleteReaction) => void
) => {
  const result = useMutation<IBookDeleteReaction>(DELETE_BOOK_REACTION, {
    update(cache, { data: { deletePost, deleteReaction } }) {
      cache.modify({
        fields: {
          posts: (existing: IPostResult, { readField }) => {
            const totalCount = existing.totalCount.value;
            const newObj: IPostResult = {
              ...existing,
              items: existing.items.filter(
                (item) => deletePost.id !== readField("id", item as any)
              ),
              totalCount: { ...existing.totalCount, value: totalCount - 1 },
            };

            return newObj;
          },
        },
      });
      cache.modify({
        fields: {
          reactions: (existing: IReactionResult, { readField }) => {
            const newObj = {
              ...existing,
              items: existing.items.filter(
                (item) => deleteReaction.id !== readField("id", item as any)
              ),
              totalCount: existing.totalCount,
            };
            return newObj;
          },
        },
      });
      cache.modify({
        fields: {
          aggregatedReactions: (
            existing: IAggregatedReaction[],
            { readField }
          ) => {
            const newArr: IAggregatedReaction[] = JSON.parse(
              JSON.stringify(existing)
            );

            newArr.length > 0 &&
              newArr?.[0].types?.[0].values.some((value) => {
                if (value.value === deleteReaction.value) {
                  value.count -= 1;
                }
              });
            return newArr;
          },
        },
      });
      cache.evict({
        id: `Post:${deletePost.id}`,
      });
      cache.evict({
        id: `Reaction:${deleteReaction.id}`,
      });
      cache.gc();
    },
    onCompleted: completedHandler,
  });
  return result;
};
