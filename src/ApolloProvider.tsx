import { createContext, useContext, useMemo, FC } from "react";
import { ApolloProvider as AP } from "@apollo/client";
import { initializeApollo } from "./apollo";

export const ApolloContext = createContext({});

interface IApolloProviderProps {
  initialApolloState: any;
}

// social url
const socialUrl = process.env.NEXT_PUBLIC_SOCIAL_URL;
// social service provider
const socialServiceProvider = process.env.NEXT_PUBLIC_SOCIAL_SERVICE_PROVIDER;

export const ApolloProvider: FC<IApolloProviderProps> = ({
  children,
  initialApolloState,
}) => {
  const store = useMemo(
    () =>
      initializeApollo(socialUrl, socialServiceProvider, initialApolloState),
    [initialApolloState]
  );

  return (
    <ApolloContext.Provider value={{}}>
      <AP client={store}>{children}</AP>
    </ApolloContext.Provider>
  );
};

const useApolloProvider = () => useContext(ApolloContext);
export default useApolloProvider;
