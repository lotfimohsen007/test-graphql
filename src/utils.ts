import {
  IAggregatedReactionsValue,
  IBookAggregatedReaction,
  IReaction,
} from "./graphql/interface";

export const calculateItemRate = (
  username: string,
  ratesListProps: IReaction[]
) => {
  let userRate = 0;
  ratesListProps.forEach((item) => {
    if (item.user.username === username) userRate = item.value;
  });
  return userRate;
};

export const calculateOverallRates = (
  bookReactions: IBookAggregatedReaction
) => {
  let overall = 0;
  let counts = 0;
  if (bookReactions.aggregatedReactions.length === 0)
    return {
      overall: 0,
      count: 0,
    };
  bookReactions.aggregatedReactions[0].types[0].values.forEach((rate) => {
    overall += rate.value * rate.count;
    counts += rate.count;
  });
  return {
    overall: overall === 0 ? 0 : (overall / counts).toFixed(1),
    count: counts,
  };
};
