import {
    ApolloClient,
    defaultDataIdFromObject,
    HttpLink,
    InMemoryCache,
    Reference,
} from '@apollo/client';

let apolloClient;
function createApolloClient(serverUrl: string, serviceProvider: string) {
    return new ApolloClient({
        ssrMode: typeof window === 'undefined',
        link: new HttpLink({
            uri: serverUrl,
            credentials: 'same-origin', // Additional fetch() options like `credentials` or `headers`
            headers: {
                serviceprovider: serviceProvider,
            },
        }),
        cache: new InMemoryCache({
            typePolicies: {
                PostsResult: {
                    fields: {
                        items(existing: Reference[], { canRead }) {
                            return existing ? existing.filter(canRead) : [];
                        },
                    },
                },
                ReactionsResult: {
                    fields: {
                        items(existing: Reference[], { canRead }) {
                            const newData = existing.filter(canRead);
                            return existing ? newData : [];
                        },
                    },
                },
            },
        }),
    });
}
export function initializeApollo(
    serverUrl: string,
    serviceProvider: string,
    initialState: any = null,
) {
    const _apolloClient = apolloClient ?? createApolloClient(serverUrl, serviceProvider);
    // If your page has Next.js data fetching methods that use Apollo Client, the initial state
    // gets hydrated here
    if (initialState) {
        // Get existing cache, loaded during client side data fetching
        const existingCache = _apolloClient.extract();
        // Restore the cache using the data passed from getStaticProps/getServerSideProps
        // combined with the existing cached data
        _apolloClient.cache.restore({ ...existingCache, ...initialState });
    }
    // For SSG and SSR always create a new Apollo Client
    if (typeof window === 'undefined') return _apolloClient;
    // Create the Apollo Client once in the client
    if (!apolloClient) apolloClient = _apolloClient;
    return _apolloClient;
}
