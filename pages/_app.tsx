import { ApolloProvider } from "../src/ApolloProvider";
import "../styles/globals.css";

function MyApp({ Component, pageProps }) {
  return (
    <ApolloProvider initialApolloState={pageProps.initialApolloState}>
      <Component {...pageProps} />
    </ApolloProvider>
  );
}

export default MyApp;
